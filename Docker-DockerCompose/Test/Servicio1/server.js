'use strict';

const express = require('express');

// Constants
const PORT = process.env.PUERTO_SERVICIO1;
const HOST = process.env.HOST_SERVICIO1;
const MENSAJE = process.env.MENSAJE;
const IP_BASEDATOS = process.env.IP_BASEDATOS;

// App
const app = express();
app.get('/test/servicio1', (req, res) => {
  res.send('Mi mensaje es: ' + MENSAJE);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
console.log('La IP de la base de datos es: ' + GetDataBaseConnection());


function GetDataBaseConnection() {
    return IP_BASEDATOS;
    // gfd
    // connect Mysql ...
}